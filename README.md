# Example project for both Java and JavaScript testing. 
Java uses Junit and JavaScript uses Karma and Jasmine
JavaScript + [Karma](http://karma-runner.github.io/) (with [Jasmine](http://jasmine.github.io/))

## Getting Started

Clone this repo with `git`:

    git clone https://gitlab.com/jravi123/tdd-tutorial/
    cd tdd-tutorial

### To run Jasmine tests follow the below instructions

Install dependencies:

	npm install
	
Run test:

	npm test

You should eventually see 1 JavaScript test ran and 0 failures.
