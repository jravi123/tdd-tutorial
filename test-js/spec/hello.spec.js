describe("hello-test", function () {
  it("has a message", function() {

	let obj = hello();
	let actual = obj.message;
	const expected = "hello!";
	
	expect(actual).toBe(expected);
  });
});