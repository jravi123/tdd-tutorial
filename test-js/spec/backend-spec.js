
describe("Spy example", function () {
	
	stub = jasmine.createSpy('stub');
	
	it("stub called", function() {
		stub('hello');
		expect(stub).toHaveBeenCalled()
	 });
  
  
	 it('fakes the loading of mbcc apis call', function(){
	  	backend = new backend();
	  	backend.loadMbccApis = jasmine.createSpy('loadMbccApis');
	  	backend.loadMbccApis();
	  	expect(backend.loadMbccApis).toHaveBeenCalled()
	 });
  
});