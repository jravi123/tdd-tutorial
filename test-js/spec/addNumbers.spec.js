
describe("Add Numbers", function () {
	
	domElements = {};
	
	beforeEach(function(){
		document.getElementById = function(ID) {
			if(domElements[ID]){
				return domElements[ID];
			}
			
			let element;
			if(ID == 'result'){
				element = document.createElement('div');
				domElements[ID]= element;
				return element;
			} else {
				element = document.createElement('input');
				domElements[ID]= element;
				return element;
			}
			return null;
		};
	});


  it("returns sum", function() {

	document.getElementById('x').value = 5;
	document.getElementById('y').value = 10;
	
	addNumbers();
	const expected = '15';
	expect(document.getElementById('result').innerHTML).toBe(expected);
  });
});