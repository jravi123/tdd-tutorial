package com.mbcc.tutorial;

public class SingletonExample {

	private static SingletonExample se = new SingletonExample();

	private SingletonExample() {

	}

	static SingletonExample getInstance() {
		return se;
	}

}
