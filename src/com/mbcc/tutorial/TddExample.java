package com.mbcc.tutorial;

import java.util.ArrayList;

public class TddExample {

	public ArrayList<String> getAllFruitsStartingWithA(String fruits) {

		ArrayList<String> returnedValue = new ArrayList<String>();

		if (fruits == null)
			return returnedValue;

		String[] tokens = fruits.split(";");
		for (String fruit : tokens) {
			if (fruit.startsWith("a")) {
				returnedValue.add(fruit);
			}
		}

		return returnedValue;
	}

}
