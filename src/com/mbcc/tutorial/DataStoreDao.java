package com.mbcc.tutorial;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;

public class DataStoreDao {

	DatastoreService datastoreService;

	public DataStoreDao(DatastoreService datastoreService) {
		this.datastoreService = datastoreService;
	}

	public DataStoreDao() {
		this.datastoreService = DatastoreServiceFactory.getDatastoreService();
	}

	public void addCustomer(Customer customer) {
		datastoreService.put(getEntity(customer));
	}

	Entity getEntity(Customer customer) {
		if (customer == null || customer.getId() == null || customer.getName() == null)
			return null;

		Entity customerEntity = new Entity("Customer");
		customerEntity.setProperty("id", customer.getId());
		customerEntity.setProperty("name", customer.getName());
		return customerEntity;
	}

}

class Customer {

	private String id;
	private String name;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}