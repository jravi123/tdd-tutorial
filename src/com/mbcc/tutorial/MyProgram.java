package com.mbcc.tutorial;

import java.util.ArrayList;

public class MyProgram {

	public static void main(String[] args) {
		String fruits = "almonds;peaches;oranges;apples;banana";
		String[] tokens = fruits.split(";");
		
		ArrayList<String> fruitsStartingWithA = new ArrayList<String>();
		
 		for(String fruit: tokens) {
			//System.out.println(fruit);
			
			if(fruit.startsWith("a")) {
				fruitsStartingWithA.add(fruit);
			}
		}
 		
 		for(String afruit : fruitsStartingWithA) {
 			System.out.println(afruit);
 		}
	}

}
