package com.mbcc.tutorial;

class Point {
	protected double x;
	protected double y;

	Point(double xCoord, double yCoord) {
		this.x = xCoord;
		this.y = yCoord;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double calculateDistance(Point topoint) {
		double dx = topoint.x - this.x;
		double dy = topoint.y - this.y;
		System.out.println("Calculating didtance between 2d points");
		return Math.sqrt(dx * dx + dy * dy);
	}
}

class Point3d extends Point {
	protected double z;

	Point3d(double xCoord, double yCoord, double zCoord) {
		super(xCoord, yCoord);
		this.z = zCoord;
	}

	public double getZ() {
		return z;
	}

	public double calculateDistance(Point topoint) {
		Point3d topoint2 = (Point3d)topoint;
		double dx = topoint2.x - super.x;
		double dy = topoint2.y - super.y;
		double dz = topoint2.z - this.z;
		System.out.println("Calculating didtance between 3d points");
		return Math.sqrt(dx * dx + dy * dy + dz * dz);
	}
}

public class Distance {

	public static void main(String[] args) {
		Point p1 = new Point(2, 2);
		Point p2 = new Point(0, 0);
		System.out.println("Distance between 2 points " + p1.calculateDistance( p2));

		Point p3 = new Point3d(0, 0, 0);
		Point3d p4 = new Point3d(2, 2, 2);
		System.out.println("Distance between 2 points " + p3.calculateDistance( p4));

	}

}