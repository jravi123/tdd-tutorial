package com.mbcc.tutorial;

import static org.junit.Assert.assertArrayEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;



public class TddExampleTest {

	TddExample tddExample = null;

	@Before
	public void setUp() {
		tddExample = new TddExample();
	}

	@Test
	public void testNullInput() {

		ArrayList<String> actual = tddExample.getAllFruitsStartingWithA(null);
		String[] emptyArray = {};
		
		assertArrayEquals(emptyArray, actual.toArray());
	}

	@Test
	public void test1AFruit() {

		String fruits = "apple";
		ArrayList<String> actual = tddExample.getAllFruitsStartingWithA(fruits);
		List<String> expected = Arrays.asList(new String[] { "apple" });
		
		assertArrayEquals(expected.toArray(), actual.toArray());
	}

	@Test
	public void test1FruitWithoutA() {
		String fruits = "banana";
		ArrayList<String> actual = tddExample.getAllFruitsStartingWithA(fruits);
		ArrayList<String> expected = new ArrayList<String>();

		assertArrayEquals(expected.toArray(), actual.toArray());
	}

	@Test
	public void test2FruitWords() {
		String fruits = "apple;banana";
		ArrayList<String> actual = tddExample.getAllFruitsStartingWithA(fruits);
		List<String> expected = Arrays.asList(new String[] { "apple" });
		
		assertArrayEquals(expected.toArray(), actual.toArray());
	}

	@Test
	public void test3FruitWords() {
		String fruits = "apple;banana;apricot";
		ArrayList<String> actual = tddExample.getAllFruitsStartingWithA(fruits);
		List<String> expected = Arrays.asList(new String[] { "apple", "apricot" });
		
		assertArrayEquals(expected.toArray(), actual.toArray());
	}

}
