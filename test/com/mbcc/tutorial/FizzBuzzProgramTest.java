package com.mbcc.tutorial;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

// This testcase is using jupiter library and note how we do not need to make the class and its method public for it to run

class FizzBuzzProgramTest {
	FizzBuzzProgram fbp = null;
	
	@BeforeEach
	void setUp() {
		fbp = new FizzBuzzProgram();
	}

	@Test
	void testGetFizz() {
		String actual = fbp.getFizzBuzzOrNumber(3);
		assertEquals(actual, "fizz");
	}
	
	@Test
	void testGetBuzz() {
		String actual = fbp.getFizzBuzzOrNumber(5);
		assertEquals(actual, "buzz");
	}
	
	@Test
	void testGetFizzBuzz() {
		String actual = fbp.getFizzBuzzOrNumber(15);
		assertEquals(actual, "fizz buzz");
	}
	
	@Test
	void testGetNumber() {
		String actual = fbp.getFizzBuzzOrNumber(16);
		assertEquals(actual, "16");
	}


}
