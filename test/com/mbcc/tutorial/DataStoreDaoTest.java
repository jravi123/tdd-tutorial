package com.mbcc.tutorial;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

public class DataStoreDaoTest {

	private static final String TEST_ID = "testId";
	private static final String TEST_NAME = "testname";
	private final LocalServiceTestHelper helper = new LocalServiceTestHelper(new LocalDatastoreServiceTestConfig());
	DatastoreService mockService;
	DataStoreDao dao;

	@Before
	public void setUp() {
		helper.setUp();
		mockService = mock(DatastoreService.class);
		dao = new DataStoreDao(mockService);
	}
		

	@Test
	public void getEntityWithValidCustomer() throws Exception {

		Customer customer = new Customer();
		customer.setId(TEST_ID);
		customer.setName(TEST_NAME);

		Entity entity = dao.getEntity(customer);

		assertEquals(entity.getProperty("id"), TEST_ID);
		assertEquals(entity.getProperty("name"), TEST_NAME);
	}

	@Test
	public void getEntityWithNullCustomer() throws Exception {
		Entity entity = dao.getEntity(null);
		assertNull(entity);
	}

	@Test
	public void addCustomer() {
		Customer customer = new Customer();
		customer.setId(TEST_ID);
		customer.setName(TEST_NAME);

		dao.addCustomer(customer);

		ArgumentCaptor<Entity> argument = ArgumentCaptor.forClass(Entity.class);
		verify(mockService).put(argument.capture());
		assertEquals(TEST_ID, argument.getValue().getProperty("id"));
		assertEquals(TEST_NAME, argument.getValue().getProperty("name"));
		assertEquals(Entity.class, argument.getValue().getClass());
	}
	
	//TODO  Add additional test cases when either or both the values are missing for customer. 

	@After
	public void tearDown() {
		helper.tearDown();
	}
}
